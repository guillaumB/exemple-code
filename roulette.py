# ce fichier abrite le code du Casino, un jeu de roulette simplifié

import os
from random import randrange
from math import ceil

# déclaration des variables de départ
argent = 1000 # on a 1000 $ au début du jeu
continuer_partie = True # booléen qui est vrai tant qu'on doit continuer la partie
nom = ""

while nom =="" or nom == " ":
    print("Bienvenu, veuillez entrer votre nom :")
    nom =input()
    
print("\nBonjour {0}, vous vous installez à la table de roulette avec {1} $.".format(nom, argent))

while continuer_partie: # tant qu'on doit continuer la partie
    # on demande à l'utilisateur d'entrer le nombre surlequel il va miser
    nombre_mise = -1
    while nombre_mise<0 or nombre_mise>49:
        print("\nEntrez le nombre sur lequel vous voulez miser (entre 0 et 49) :")
        nombre_mise = input()
        # on convertit le nombre misé
        try:
            nombre_mise = int(nombre_mise)
        except ValueError:
            print("\nVous n'avez pas entré de nombre")
            nombre_mise = -1
            continue
        if nombre_mise<0:
            print("\nCe nombre est négatif")
        if nombre_mise>49:
            print("\nCe nombre est supérieur à 49")

    # à présent, on sélectionne la somme à miser sur le nombre
    mise = 0
    while mise<=0 or mise>argent:
        print("\nEntrez le montant de votre mise :")
        mise = input()
        # on converti la mise
        try:
            mise = int(mise)
        except ValueError:
            print("\nVous n'avez pas entré de nombre")
            mise = -1
            continue
        if mise<=0:
            print("\nLa mise entrée est négative ou nulle.")
        if mise>argent:
            print("\nVous ne pouvez miser autant, vous n'avez que", argent, "$")

    # le nombre misé et la mise ont été sélectionnés par l'utilisateur
    # on fait tourner la roulette
    numero_gagnant = 10#randrange(50)
    print("\nLa roulette tourne... ... et s'arrête sur le numéro", numero_gagnant)

    # on va établir le gain du joueur
    if numero_gagnant == nombre_mise:
        print("\n"+"Félicitations".upper()+" {0} ! Vous obtenez {1} $ !".format(nom, mise*3))
        argent += mise * 3
    elif numero_gagnant %2 == nombre_mise %2: # ils sont de la même couleur
        mise = ceil(mise * 0.5)
        print("\nVous avez misé sur la bonne couleur. Vous obtenez", mise, "$")
        argent += mise
    else:
        print("\nDésolé "+nom+",  c'est pas pour cette fois. Vous perdez votre mise.")
        argent -= mise

    # on va interrompre la partie si le joueur est ruiné
    if argent<=0:
        print("\nVous êtes ruiné ! C'est la fin de la partie.")
        continuer_partie = False
    else:
        # on affiche l'argent du joueur
        print("\nVous avez à présent", argent, "$")
        print("\nSouhaitez-vous quitter le casino (Y/n) ?")
        quitter = input()
        if quitter.lower() ==  "y":
            print("\nVous quittez le casino avec ", argent)
            continuer_partie = False

# on met en pause le système (Windows)
os.system("pause")