<?php

include('config.php');

//connection à la base de données
$bdd = new PDO("mysql:host=localhost;dbname=".DBNAME, USER, PASS);

//on récupère le type d'action à effectué
$action = $_POST['action'];

if($action == 'users')
{
    echo ListUsers();
}
elseif($action == 'form')
{
    //récupération des valeurs
    $id = $_POST['id'];
    
    echo FormUser($id);
}
elseif($action == 'update')
{
    //récupération des valeurs
    $id = $_POST['id'];
    $civil = $_POST['civil'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $ville = $_POST['ville'];
    $tel = $_POST['tel'];
    $pseudo = $_POST['pseudo'];
    $pass = $_POST['pass'];
    
    UpdateUser($id, $nom, $prenom, $ville, $civil, $tel, $pass, $pseudo);
}
elseif($action == 'insert')
{
    //récupération des valeurs
    $civil = $_POST['civil'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $ville = $_POST['ville'];
    $tel = $_POST['tel'];
    $pseudo = $_POST['pseudo'];
    $pass = $_POST['pass'];

    InsertUser($nom, $prenom, $ville, $civil, $tel, $pass, $pseudo);
}
elseif($action == 'suppr')
{
    //suppression de l'utilisateur
    $id = $_POST['id'];
    
    DelUser($id);
}
else
{
    echo "Action Non connu...";
}

//fonctions

function ListUsers()
{
    //liste tous les utilisateurs
    global $bdd;

    $utf = $bdd->query('SET NAMES utf8;');
    
    $req = 'SELECT * FROM users ORDER BY id ASC;';
    $result = $bdd->query($req);
    
    $content = '<ul id="list">';    
    while($data = $result->fetch())
    {
        $content .= '<li class="user" id="'.$data['id'].'">'.$data['id'].' - '.$data['name'].' '.$data['fname'].'</li>';
    }    
    $content .= '</ul>';
    
    return $content;
}

function FormUser($id)
{
    //Rempli le formulaire de modification
    global $bdd;
    
    $utf = $bdd->query('SET NAMES utf8;');
    
    $req = 'SELECT * FROM users WHERE id='.$id.';';
    $result = $bdd->query($req);
    
    while($data = $result->fetch())
    {
        ?>
        <tr>
            <td>ID :</td>
            <td id="id"><?php echo $data['id']; ?></td>
        </tr>
        <tr>
            <td>Civilité :</td>
            <td>
                <select name="civilite" id="civilite">
                    <option value="Mr" <?php if($data['type']=="Mr"){echo 'selected="selected"';}?>>Mr</option>
                    <option value="Mme" <?php if($data['type']=="Mme"){echo 'selected="selected"';}?>>Mme</option>
                    <option value="Mlle" <?php if($data['type']=="Mlle"){echo 'selected="selected"';}?>>Mlle</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nom :</td>
            <td><input type="text" name="nom" value="<?php echo $data['name']; ?>" id="nom"></td>
        </tr>
        <tr>
            <td>Prénom :</td>
            <td><input type="text" name="prenom" value="<?php echo $data['fname']; ?>" id="prenom"></td>
        </tr>
        <tr>
            <td>Ville :</td>
            <td><input type="text" name="ville" value="<?php echo $data['city']; ?>" id="ville"></td>
        </tr>
        <tr>
            <td>Téléphone :</td>
            <td><input type="tel" name="tel" value="<?php echo $data['phone']; ?>" id="tel"></td>
        </tr>
        <tr>
            <td>Pseudo :</td>
            <td><input type="text" name="pseudo" value="<?php echo $data['login']; ?>" id="pseudo"></td>
        </tr>
        <tr>
            <td>Password :</td>
            <td><input type="password" name="pass" value="<?php echo $data['password']; ?>" id="pass"></td>
        </tr>
        <!--<tr>
            <td colspan="2" style="text-align: center;"><input type="button" value="Modifier" id="update"></td>
        </tr>-->
        <?php
    }
}

function UpdateUser($id, $name, $fname, $city, $type, $phone, $pass, $login)
{
    //met à jour les informations d'un utilisateur
    global $bdd;
    
    $utf = $bdd->query('SET NAMES utf8;');
    
    $req = 'UPDATE users SET name="'.$name.'", fname="'.$fname.'", city="'.$city.'", type="'.$type.'", phone="'.$phone.'", password="'.$pass.'", login="'.$login.'" WHERE id="'.$id.'";';
    $result = $bdd->query($req);
}

function InsertUser($name, $fname, $city, $type, $phone, $pass, $login)
{
    //ajoute un utilisateur à la base de données
    global $bdd;
    
    $utf = $bdd->query('SET NAMES utf8;');
    
    $req = 'INSERT INTO users(name, fname, city, type, phone, login, password) VALUES("'.$name.'", "'.$fname.'", "'.$city.'", "'.$type.'", "'.$phone.'","'.$login.'","'.$pass.'");';
    $result = $bdd->query($req);
}

function DelUser($id)
{
    //supprime un utilisateur
    global $bdd;
    
    $req = 'DELETE FROM users WHERE id='.$id.';';
    $result = $bdd->query($req);
}
?>