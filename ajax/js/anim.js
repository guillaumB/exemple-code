$(document).ready(function(){
    $("#new-user").hide();
    
    $("#add").click(function(){
        $("#modif-user").hide();
        $("#new-user").fadeIn("slow");
    });
    
    $("#change").click(function(){
        $("#modif-user").fadeIn("slow");
        $("#new-user").hide();
    });
    
    //drag 'n' drop
    $(".user").draggable();
});