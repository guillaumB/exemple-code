$(document).ready(function(){
    //chargement initial des users
    $("#users").load("data.php", {action: "users"},function(){ $(".user").draggable({revert: true})});

    //clique sur modifier (dans modifier utilisateur)
    $("#form-modif input").live('keyup', function(){
        $.ajax("data.php", {
            type: 'POST',
            data: {
                action: 'update',
                id: $("#id").text(),
                civil: $("#civilite").attr('value'), 
                nom: $("#nom").val(),
                prenom: $("#prenom").val(),
                ville: $("#ville").val(),
                tel: $("#tel").val(),
                pseudo: $("#pseudo").val(),
                pass: $("#pass").val()
            }
        }).done(function(){                
                //on met à jour la liste des users
                $("#users").load("data.php", {action: "users"},function(){ $(".user").draggable({revert: true})});
            });
    });
    
    $("#civilite").live('change', function(){
        $.ajax("data.php", {
            type: 'POST',
            data: {
                action: 'update',
                id: $("#id").text(),
                civil: $("#civilite").attr('value'), //problem avec la civilité... elle ne change pas...
                nom: $("#nom").val(),
                prenom: $("#prenom").val(),
                ville: $("#ville").val(),
                tel: $("#tel").val(),
                pseudo: $("#pseudo").val(),
                pass: $("#pass").val()
            }
        }).done(function(){                
                //on met à jour la liste des users
                $("#users").load("data.php", {action: "users"},function(){ $(".user").draggable({revert: true})});
            });
    });

    //clique sur ajouter (dans nouveau utilisateur)
    $("#submit").click(function(){
        $.ajax("data.php", {
            type: 'POST',
            data: {
                action: 'insert',
                civil: $("#ncivilite").attr('value'),
                nom: $("#nnom").val(),
                prenom: $("#nprenom").val(),
                ville: $("#nville").val(),
                tel: $("#ntel").val(),
                pseudo: $("#npseudo").val(),
                pass: $("#npass").val()
            }
        }).done(function(){
                alert("Utilisateur Ajouté !");
                
                //reset du formumaire
                $("#nnom").attr("value", "");
                $("#nprenom").attr("value", "");
                $("#nville").attr("value", "");
                $("#ntel").attr("value", "");
                $("#npseudo").attr("value", "");
                $("#npass").attr("value", "");         
                
                //on met à jour la liste des utilisateurs
                $("#users").load("data.php", {action: "users"},function(){ $(".user").draggable({revert: true})});
            });
    });
    
    //Clique sur un utilisateur de la liste
    $(".user").live('click', function(){
        $("#modif-user").fadeIn("slow");
        $("#new-user").hide();
        $("#form-modif").load("data.php", {action: "form", id: $(this).attr('id')});
        
        var position = $(this).position();
        
        $("#main").animate({
            top: position.top+40
        });
    });
    
    //Drop
    $("#corbeille").droppable({tolerance: 'pointer', drop: function(event, ui){
            $.ajax("data.php", {
                type: 'POST',
                data: {
                    action: 'suppr',
                    id: $(ui.draggable).attr("id")
                }
            });
            $(ui.draggable).remove();
        }
    });    
});