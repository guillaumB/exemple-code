-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 13 Février 2012 à 00:06
-- Version du serveur: 5.1.49
-- Version de PHP: 5.3.3-7+squeeze3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `basesrc10f02`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `city` varchar(45) NOT NULL,
  `type` varchar(4) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `password` varchar(4) NOT NULL,
  `login` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=175 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `fname`, `city`, `type`, `phone`, `password`, `login`) VALUES
(1, 'ANTOINE', 'Patrick', 'Troyes', 'Mlle', '03.25.75.59.76', '793f', 'p.antoine'),
(2, 'ARGANINI', 'Gérald', 'Saint Parres', 'Mlle', '03.25.75.59.03', 'aab1', 'g.arganini'),
(3, 'ARNAUD', 'Jean-Michel', 'Verrières', 'Mlle', '03.25.75.59.12', '18b0', 'j.arnaud'),
(4, 'ARONICA', 'Aude', 'Lusigny', 'Mlle', '03.25.21.59.27', '53ce', 'a.aronica'),
(5, 'AUBERT', 'Florianne', 'Pruny', 'Mlle', '03.25.21.59.26', '91a7', 'f.aubert'),
(6, 'BARON', 'Laurent', 'Géraudot', 'Mr', '03.25.21.59.25', '757b', 'l.baron'),
(7, 'BARRIÈRE', 'Guillaume', 'Troyes', 'Mr', '03.25.75.59.25', 'bec6', 'g.barrière'),
(8, 'BARTHELEMY', 'David', 'Saint André', 'Mr', '03.25.75.59.02', '6cc7', 'd.barthelemy'),
(9, 'BAUSER', 'Fanny', 'Troyes', 'Mr', '03.25.75.59.11', '709c', 'f.bauser'),
(10, 'BELLOIR', 'Guillaum', 'Saint Parres', 'Mr', '03.25.21.59.14', '75b1', 'g.belloir'),
(11, 'BEUGNOT', 'Karen', 'Verrières', 'Mlle', '03.25.21.59.02', 'd9e8', 'k.beugnot'),
(12, 'BIENAIME', 'Paul', 'Lusigny', 'Mr', '03.25.75.59.88', 'e604', 'p.bienaime'),
(13, 'BILLARD', 'Sonia', 'Pruny', 'Mlle', '03.25.75.59.01', 'aab1', 's.billard'),
(14, 'BINANT', 'Lucie', 'Géraudot', 'Mlle', '03.25.75.59.87', '411a', 'l.binant'),
(15, 'BLONDEL', 'Fatima', 'Troyes', 'Mlle', '03.25.21.59.40', '6742', 'f.blondel'),
(16, 'BOULIN', 'Germain', 'Saint André', 'Mme', '03.25.21.59.01', '9f4f', 'g.boulin'),
(17, 'BOUVRON', 'Denis', 'Troyes', 'Mme', '03.25.75.59.75', 'd2a8', 'd.bouvron'),
(18, 'BROUILLARD', 'Elise', 'Saint Parres', 'Mlle', '03.25.21.59.39', 'fe2e', 'e.brouillard'),
(19, 'BUI', 'Brice', 'Verrières', 'Mlle', '03.25.75.59.24', '6049', 'b.bui'),
(20, 'CAMILLINI', 'Stéphanie', 'Lusigny', 'Mlle', '03.25.21.59.24', 'e5f7', 's.camillini'),
(21, 'CAPELA', 'Steven', 'Pruny', 'Mr', '03.25.75.59.23', '9f4f', 's.capela'),
(22, 'CARRÉ', 'Jean-Nöel', 'Géraudot', 'Mr', '03.25.75.59.36', '4d0d', 'j.carré'),
(23, 'CHAN', 'Sylvain', 'Troyes', 'Mme', '03.25.75.59.08', '31e0', 's.chan'),
(24, 'CHARIGNON', 'Philippe', 'Saint André', 'Mme', '03.25.75.59.74', '419d', 'p.charignon'),
(25, 'CHÉRAIN', 'Abdelazziz', 'Pruny', 'Mme', '03.25.75.59.73', 'a4ed', 'a.chérain'),
(26, 'CHIBOUT', 'Estela', 'Géraudot', 'Mme', '03.25.75.59.62', '419d', 'e.chibout'),
(27, 'CLAVERIE', 'Elodie', 'Troyes', 'Mr', '03.25.75.59.00', '0a41', 'e.claverie'),
(28, 'CLEMONT', 'Puthy', 'Saint André', 'Mr', '03.25.75.59.86', 'b0c6', 'p.clemont'),
(29, 'COLAS', 'Habib', 'Troyes', 'Mme', '03.25.21.59.38', '448c', 'h.colas'),
(30, 'COLLARD', 'Marie Jeanne', 'Saint Parres', 'Mme', '03.25.75.59.44', '6049', 'm.collard'),
(31, 'COLSON', 'Jean-Philippe', 'Troyes', 'Mme', '03.25.75.59.85', '89fc', 'j.colson'),
(32, 'COLY', 'Aude', 'Saint Parres', 'Mlle', '03.25.75.59.22', '1e64', 'a.coly'),
(33, 'COMANDINI', 'Marc', 'Verrières', 'Mlle', '03.25.75.59.72', 'f5e6', 'm.comandini'),
(34, 'COURRET', 'Nicolas', 'Lusigny', 'Mr', '03.25.75.59.71', '8350', 'n.courret'),
(35, 'COUSTILLET', 'Nicolas', 'Pruny', 'Mme', '03.25.21.59.23', '86ff', 'n.coustillet'),
(36, 'COUTROT', 'Karim', 'Géraudot', 'Mr', '03.25.21.59.13', '1a75', 'k.coutrot'),
(37, 'CROSSETTE', 'Nicolas', 'Troyes', 'Mlle', '03.25.75.59.21', '85fd', 'n.crossette'),
(38, 'DAGORN', 'Karine', 'Saint André', 'Mme', '03.25.75.59.01', '02fc', 'k.dagorn'),
(39, 'DAGUET', 'Elodie', 'Pruny', 'Mme', '03.25.75.59.47', '8813', 'e.daguet'),
(40, 'DANGIN', 'Nicolas', 'Géraudot', 'Mr', '03.25.75.59.10', 'a602', 'n.dangin'),
(41, 'DELOYE', 'Sylvie', 'Saint André', 'Mlle', '03.25.75.59.98', 'e6cf', 's.deloye'),
(42, 'DENG', 'Nicole', 'Troyes', 'Mme', '03.25.75.59.61', '343b', 'n.deng'),
(43, 'DHAM', 'Kenan', 'Saint Parres', 'Mr', '03.25.75.59.84', '7273', 'k.dham'),
(44, 'DIEHL', 'Audrey', 'Troyes', 'Mr', '03.25.75.59.55', '6560', 'a.diehl'),
(45, 'DINQUEL', 'Aurélien', 'Saint Parres', 'Mme', '03.25.21.59.12', 'c1d2', 'a.dinquel'),
(46, 'DUBERY', 'Catherine', 'Verrières', 'Mr', '03.25.75.59.60', 'e657', 'c.dubery'),
(47, 'DUFOUR', 'Marc', 'Lusigny', 'Mr', '03.25.75.59.83', 'de1d', 'm.dufour'),
(48, 'ESCHARD', 'Jean-philippe', 'Pruny', 'Mlle', '03.25.75.59.43', '365d', 'j.eschard'),
(49, 'ESSIG', 'Cédric', 'Troyes', 'Mme', '03.25.75.59.41', 'ce18', 'c.essig'),
(50, 'ESSOUNBOULI', 'François', 'Saint André', 'Mme', '03.25.75.59.38', '27e7', 'f.essounbouli'),
(51, 'FAVRE', 'Celine', 'Troyes', 'Mr', '03.25.75.59.34', '563b', 'c.favre'),
(52, 'FERRIER', 'Thanoulom', 'Saint Parres', 'Mlle', '03.25.75.59.97', '4170', 't.ferrier'),
(53, 'FERU', 'Jean-Vincent', 'Troyes', 'Mr', '03.25.21.59.11', '9e3e', 'j.feru'),
(54, 'FISCH', 'Kim Pascal', 'Saint Parres', 'Mme', '03.25.75.59.10', 'd306', 'k.fisch'),
(55, 'FLIPON', 'Raphaël', 'Verrières', 'Mme', '03.25.75.59.02', '4d13', 'r.flipon'),
(56, 'FONTAINE', 'Elske', 'Lusigny', 'Mlle', '03.25.21.59.10', '7895', 'e.fontaine'),
(57, 'FORT', 'Fabrice', 'Pruny', 'Mlle', '03.25.21.59.09', '4d13', 'f.fort'),
(58, 'FOUCHARD', 'Alexandre', 'Géraudot', 'Mr', '03.25.75.59.70', '9d4f', 'a.fouchard'),
(59, 'FROMONT', 'Olivier', 'Troyes', 'Mr', '03.25.21.59.37', '6cc7', 'o.fromont'),
(60, 'GAMBLIN', 'autre', 'Saint André', 'Mr', '03.25.75.59.33', '81ed', 'a.gamblin'),
(61, 'GARNEROT', 'Hélène', 'Pruny', 'Mlle', '03.25.75.59.20', '9571', 'h.garnerot'),
(62, 'GAUTHIER', 'Rémi', 'Géraudot', 'Mlle', '03.25.75.59.19', 'afb1', 'r.gauthier'),
(63, 'GHEDJATI', 'Guillaume', 'Saint André', 'Mr', '03.25.21.59.22', 'ad89', 'g.ghedjati'),
(64, 'GILABERT', 'Hervé', 'Troyes', 'Mlle', '03.25.75.59.03', '1fdd', 'h.gilabert'),
(65, 'GLIKMAN', 'Alexandre', 'Saint Parres', 'Mme', '03.25.75.59.82', '6330', 'a.glikman'),
(66, 'GOBERT', 'Mario', 'Troyes', 'Mlle', '03.25.21.59.36', 'e657', 'm.gobert'),
(67, 'GODEFERT', 'Baptiste', 'Saint Parres', 'Mme', '03.25.75.59.32', '757b', 'b.godefert'),
(68, 'GOMMERY', 'Grégory', 'Verrières', 'Mr', '03.25.75.59.04', 'e6cf', 'g.gommery'),
(69, 'GRANDVEAU', 'Thierry', 'Lusigny', 'Mr', '03.25.75.59.96', 'ad89', 't.grandveau'),
(70, 'GRASSET', 'Alexis', 'Pruny', 'Mlle', '03.25.21.59.21', '5a04', 'a.grasset'),
(71, 'GRENET', 'Grégory', 'Troyes', 'Mme', '03.25.21.59.35', '4170', 'g.grenet'),
(72, 'GROSDOIT', 'Guillaume', 'Saint André', 'Mme', '03.25.75.59.63', '6d1f', 'g.grosdoit'),
(73, 'GUGLIELMETTI', 'Céline', 'Troyes', 'Mme', '03.25.21.59.41', '952e', 'c.guglielmetti'),
(74, 'GUILLEMIN', 'Jérôme', 'Saint Parres', 'Mme', '03.25.75.59.18', '2af6', 'j.guillemin'),
(75, 'GUINOT', 'Marion', 'Troyes', 'Mlle', '03.25.75.59.95', 'ce18', 'm.guinot'),
(76, 'HADJ HENNI', 'Céline', 'Saint Parres', 'Mlle', '03.25.75.59.69', 'c005', 'c.hadj henni'),
(77, 'HALLER', 'Wissam', 'Verrières', 'Mlle', '03.25.75.59.17', '1fdd', 'w.haller'),
(78, 'HAMZAOUI', 'Jean-Marie', 'Lusigny', 'Mme', '03.25.75.59.53', 'e9fa', 'j.hamzaoui'),
(79, 'HANNANE', 'Thomas', 'Pruny', 'Mme', '03.25.75.59.51', '6d1f', 't.hannane'),
(80, 'HAY', 'Rémi', 'Géraudot', 'Mr', '03.25.75.59.05', '709c', 'r.hay'),
(81, 'HEBERT', 'Mathieu', 'Troyes', 'Mr', '03.25.21.59.34', '563b', 'm.hebert'),
(82, 'HERBELOT', 'Aline', 'Saint André', 'Mr', '03.25.75.59.50', 'b93f', 'a.herbelot'),
(83, 'HOUILLON', 'Emeric', 'Pruny', 'Mr', '03.25.75.59.09', '03f6', 'e.houillon'),
(84, 'HUN', 'Viengthong', 'Géraudot', 'Mr', '03.25.75.59.94', '75b1', 'v.hun'),
(85, 'JACQUELIN', 'Kinh-Kha', 'Saint André', 'Mlle', '03.25.75.59.06', '8ced', 'k.jacquelin'),
(86, 'JANIN', 'Anne', 'Troyes', 'Mme', '03.25.75.59.59', '594c', 'a.janin'),
(87, 'JURADO', 'Renaud', 'Saint Parres', 'Mme', '03.25.75.59.52', '6742', 'r.jurado'),
(88, 'KAR', 'Christophe', 'Troyes', 'Mlle', '03.25.21.59.20', '5c12', 'c.kar'),
(89, 'LAFILLE', 'Cindy', 'Saint Parres', 'Mr', '03.25.75.59.58', '356b', 'c.lafille'),
(90, 'LAIGRE', 'Emilie', 'Verrières', 'Mme', '03.25.75.59.08', '793f', 'e.laigre'),
(91, 'LAMY', 'Vincent', 'Lusigny', 'Mme', '03.25.21.59.08', 'eb66', 'v.lamy'),
(92, 'LAVECHIN', 'Johanna', 'Pruny', 'Mr', '03.25.75.59.07', 'f101', 'j.lavechin'),
(93, 'LE', 'Olivier', 'Troyes', 'Mme', '03.25.75.59.37', 'd2a8', 'o.le'),
(94, 'LEMAITRE', 'Olivier', 'Saint André', 'Mlle', '03.25.75.59.31', 'fe2e', 'o.lemaitre'),
(95, 'LEROY', 'Anne Sophie', 'Troyes', 'Mr', '03.25.21.59.19', 'c497', 'a.leroy'),
(96, 'LEVY LECUIVRE', 'Benjamin', 'Saint Parres', 'Mlle', '03.25.75.59.81', '411a', 'b.levy lecuivre'),
(97, 'LOMBRICI', 'Olivier', 'Troyes', 'Mlle', '03.25.75.59.68', '0a41', 'o.lombrici'),
(98, 'LOUIS', 'Maxime', 'Saint Parres', 'Mme', '03.25.21.59.33', '952e', 'm.louis'),
(99, 'LUANGKHOT', 'Erwan', 'Verrières', 'Mr', '03.25.75.59.16', 'e604', 'e.luangkhot'),
(100, 'MACHKOURI', 'Fabrice', 'Lusigny', 'Mr', '03.25.75.59.45', 'afb1', 'f.machkouri'),
(101, 'MAGLOIRE', 'Félix', 'Pruny', 'Mlle', '03.25.75.59.15', 'a01e', 'f.magloire'),
(102, 'MAMONE', 'Florian', 'Géraudot', 'Mme', '03.25.75.59.67', '98a7', 'f.mamone'),
(103, 'MANJARD', 'Lamine', 'Troyes', 'Mr', '03.25.75.59.92', '39f7', 'l.manjard'),
(104, 'MARCHAL', 'Ingrid', 'Saint André', 'Mr', '03.25.75.59.14', '6e21', 'i.marchal'),
(105, 'MARLIER', 'Patrice', 'Pruny', 'Mr', '03.25.75.59.07', '8813', 'p.marlier'),
(106, 'MAROT', 'Guillaume', 'Géraudot', 'Mlle', '03.25.75.59.06', 'eb66', 'g.marot'),
(107, 'MARTINET', 'Laure', 'Saint André', 'Mme', '03.25.75.59.80', '53ce', 'l.martinet'),
(108, 'MASSÉ', 'Jean-François', 'Troyes', 'Mme', '03.25.21.59.07', '21e5', 'j.massé'),
(109, 'MÉLÉ', 'Michel', 'Saint Parres', 'Mr', '03.25.21.59.32', 'c005', 'm.mélé'),
(110, 'MEUZERET', 'Alexandre', 'Troyes', 'Mr', '03.25.21.59.06', '97b7', 'a.meuzeret'),
(111, 'MILLET', 'Géraldine', 'Saint André', 'Mr', '03.25.75.59.30', 'e5f7', 'g.millet'),
(112, 'MILLOT', 'Anne-Lise', 'Troyes', 'Mme', '03.25.75.59.79', '828a', 'a.millot'),
(113, 'MOINELET', 'Corinne', 'Saint Parres', 'Mme', '03.25.21.59.05', '8350', 'c.moinelet'),
(114, 'MORAIS', 'Patrice', 'Troyes', 'Mme', '03.25.75.59.78', '7895', 'p.morais'),
(115, 'MORDIN', 'Sandro', 'Saint Parres', 'Mlle', '03.25.21.59.04', 'a01e', 's.mordin'),
(116, 'MOUCHEL', 'José', 'Verrières', 'Mme', '03.25.21.59.03', '344c', 'j.mouchel'),
(117, 'NAMORY', 'Julie', 'Lusigny', 'Mlle', '03.25.75.59.91', '582b', 'j.namory'),
(118, 'NGUYEN', 'Damien', 'Pruny', 'Mlle', '03.25.75.59.05', '86ff', 'd.nguyen'),
(119, 'NGUYEN', 'laure', 'Géraudot', 'Mlle', '03.25.75.59.05', '1e64', 'l.nguyen'),
(120, 'NIVET', 'Francois', 'Troyes', 'Mr', '03.25.75.59.93', '47a7', 'f.nivet'),
(121, 'PASQUIER', 'Vincent', 'Saint André', 'Mlle', '03.25.75.59.57', 'bec6', 'v.pasquier'),
(122, 'PICARD', 'Laure', 'Pruny', 'Mr', '03.25.21.59.18', '6560', 'l.picard'),
(123, 'PITOIS', 'Anthony', 'Géraudot', 'Mlle', '03.25.75.59.29', 'b179', 'a.pitois'),
(124, 'PITOIS', 'Jean-Marc', 'Saint André', 'Mr', '03.25.75.59.29', '73ec', 'j.pitois'),
(125, 'POTHION', 'Julien', 'Troyes', 'Mlle', '03.25.75.59.09', '08a6', 'j.pothion'),
(126, 'QUAN', 'Julien', 'Saint Parres', 'Mr', '03.25.21.59.17', '1a4a', 'j.quan'),
(127, 'RECCHIA', 'Adel', 'Troyes', 'Mlle', '03.25.21.59.31', 'fed9', 'a.recchia'),
(128, 'RECCHIA', 'Fabien', 'Saint Parres', 'Mlle', '03.25.21.59.31', 'b0c6', 'f.recchia'),
(129, 'RESKA', 'Damien', 'Verrières', 'Mlle', '03.25.21.59.30', '85fd', 'd.reska'),
(130, 'ROGER', 'Sébastien', 'Lusigny', 'Mr', '03.25.21.59.29', '91a7', 's.roger'),
(131, 'ROMANENS', 'Damien', 'Pruny', 'Mr', '03.25.75.59.13', 'a602', 'd.romanens'),
(132, 'SABBOURI', 'Vincent', 'Troyes', 'Mr', '03.25.75.59.28', '448c', 'v.sabbouri'),
(133, 'SABINI', 'Julien', 'Saint André', 'Mme', '03.25.75.59.27', 'ddfd', 'j.sabini'),
(134, 'SANCHEZ', 'Najib', 'Troyes', 'Mme', '03.25.75.59.48', '5c12', 'n.sanchez'),
(135, 'SANDRÉ', 'Vivien', 'Saint Parres', 'Mme', '03.25.75.59.40', '9571', 'v.sandré'),
(136, 'SCHWARTZ', 'Laure', 'Troyes', 'Mme', '03.25.75.59.90', 'c1d2', 'l.schwartz'),
(137, 'SE', 'Anthony', 'Saint Parres', 'Mlle', '03.25.21.59.28', 'c52b', 'a.se'),
(138, 'SERRES', 'Laurent', 'Verrières', 'Mr', '03.25.21.59.16', '81ed', 'l.serres'),
(139, 'SIMONNOT', 'Nicolas', 'Lusigny', 'Mlle', '03.25.75.59.77', '356b', 'n.simonnot'),
(140, 'SUAREZ', 'Patrice', 'Pruny', 'Mlle', '03.25.75.59.54', '03f6', 'p.suarez'),
(141, 'THOMAS', 'Benoit', 'Géraudot', 'Mr', '03.25.75.59.39', 'f5e6', 'b.thomas'),
(142, 'TOCUT', 'Arnaud', 'Troyes', 'Mr', '03.25.75.59.26', '8ced', 'a.tocut'),
(143, 'TORD', 'Daniele', 'Saint André', 'Mme', '03.25.75.59.56', '343b', 'd.tord'),
(144, 'TRAN', 'Sébastien', 'Pruny', 'Mme', '03.25.75.59.89', '98a7', 's.tran'),
(145, 'TRHIN', 'Sébastien', 'Géraudot', 'Mr', '03.25.75.59.42', '47a7', 's.trhin'),
(146, 'VACHON', 'Shui', 'Saint André', 'Mme', '03.25.75.59.04', '27e7', 's.vachon'),
(147, 'VERFAILLIE', 'Brahim', 'Troyes', 'Mme', '03.25.75.59.64', 'de1d', 'b.verfaillie'),
(148, 'VIVET', 'Arnaud', 'Saint Parres', 'Mme', '03.25.75.59.46', '39f7', 'a.vivet'),
(149, 'WILK', 'Julien', 'Troyes', 'Mlle', '03.25.21.59.15', '42f5', 'j.wilk'),
(158, 'coucou', 'Carla', 'Nogent', 'Mr', '000000', 'xxx', 'login'),
(171, 'Dupont', 'Monsieur', '', 'Mlle', '', '', ''),
(172, 'Dupont', 'madame', '', 'Mlle', '', '', ''),
(173, 'madame', '', '', 'Mme', '', '', ''),
(174, 'monsieur', '', '', 'Mr', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
