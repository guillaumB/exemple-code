<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Admin - St Pavu</title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" >
        
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/load.js"></script>
    <script type="text/javascript" src="js/anim.js"></script>
</head>
<body>
    <header  id="top">
        <p>Admin - St Pavu</p>
    </header>
    
    <div id="global">
        <div id="list-users" class="design">
            <header>
                <p>Users</p>
            </header>
            <section id="users" class="content">
                
            </section>  
        </div>
        
        <section id="main">
            <div id="shortcut">
                <img src="img/trash.png" alt="Corbeille" id="corbeille" title="Poubelle">
                <img src="img/add_user.png" alt="Add User" id="add" title="Ajouter un Utilisateur">
                <img src="img/modif_user.png" alt="Modif User" id="change" title="Modifier un Utilisateur">
            </div>
            
            <div id="modif-user" class="design">
                <header>
                    <p>Modification</p>
                </header>
                <section id="modif" class="content">
                    <p>
                        Vous pouvez modifier les informations concernant l'utilisateur suivant :
                    </p>                        
                    <table id="form-modif">
                        <tr>
                            <td>ID :</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Civilité :</td>
                            <td>
                                <select name="civilite" id="civilite">
                                    <option value="Mr">Mr</option>
                                    <option value="Mme">Mme</option>
                                    <option value="Mlle">Mlle</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Nom :</td>
                            <td><input type="text" name="nom"></td>
                        </tr>
                        <tr>
                            <td>Prénom :</td>
                            <td><input type="text" name="prenom"></td>
                        </tr>
                        <tr>
                            <td>Ville :</td>
                            <td><input type="text" name="ville"></td>
                        </tr>
                        <tr>
                            <td>Téléphone :</td>
                            <td><input type="tel" name="tel"></td>
                        </tr>
                        <tr>
                            <td>Pseudo :</td>
                            <td><input type="text" name="pseudo"></td>
                        </tr>
                        <tr>
                            <td>Password :</td>
                            <td><input type="password" name="pass"></td>
                        </tr>
                        <!--<tr>
                            <td colspan="2" style="text-align: center;"><input type="button" value="Modifier" id="update"></td>
                        </tr>-->
                    </table>
                </section> 
            </div>
            
            <div id="new-user" class="design">
                <header>
                    <p>Nouveau utilisateur</p>
                </header>
                <section id="new" class="content">
                    <p>
                        Veuillez entrer les informations pour le nouveau utilisateur :
                    </p>
                    
                    <table>
                        <tr>
                            <td>Civilité :</td>
                            <td>
                                <select name="civilite" id="ncivilite">
                                    <option value="Mr">Mr</option>
                                    <option value="Mme">Mme</option>
                                    <option value="Mlle">Mlle</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Nom :</td>
                            <td><input type="text" name="nom" id="nnom"></td>
                        </tr>
                        <tr>
                            <td>Prénom :</td>
                            <td><input type="text" name="prenom" id="nprenom"></td>
                        </tr>
                        <tr>
                            <td>Ville :</td>
                            <td><input type="text" name="ville" id="nville"></td>
                        </tr>
                        <tr>
                            <td>Téléphone :</td>
                            <td><input type="tel" name="tel" id="ntel"></td>
                        </tr>
                        <tr>
                            <td>Pseudo :</td>
                            <td><input type="text" name="pseudo" id="npseudo"></td>
                        </tr>
                        <tr>
                            <td>Password :</td>
                            <td><input type="password" name="pass" id="npass"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><input type="button" value="Ajouter" id="submit"></td>
                        </tr>
                    </table>              
                </section>            
            </div>
        </section>
    </div>
</body>
</html>