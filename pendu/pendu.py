"""Ce fichier contient le jeu du pendu"""

import os
from fonctions import * #import du fichier fonctions
from donnees import * # import du fichier donnees.py

# message d'accueil
print("Bienvenu dans le jeu du Pendu !".center(70))

joueur = select_joueur()
#score = get_score()
#score_joueur = show_score(joueur, score)

continuer = "y"


while continuer == "y":
    coup = 0
    mot_a_trouver = select_mot()
    lettre_entrer = []
    mot_hidden = cache_mot(mot_a_trouver, lettre_entrer)
       
    while mot_a_trouver != mot_hidden:
        print("\n"+mot_hidden)
         
        lettre_joueur = saisir_lettre()
        lettre_entrer.append(lettre_joueur)
        
        mot_hidden = cache_mot(mot_a_trouver, lettre_entrer)
        
        coup = coup + 1
    
    print("\nBravo",joueur,"!")
    print("\nVous avez trouvé le mot en",coup,"coups !")
    
    
    continuer = input("\souhaitez vous rejouez (Y/n)")
    continuer =continuer.lower()
    
# save_score(score)

os.system("pause")