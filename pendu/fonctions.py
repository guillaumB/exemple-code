"""Ce fichier contient les fonctions dont le jeu a besoin"""
import os
import pickle
from donnees import * # import du fichier donnees.py
from random import * # import du module random

def select_joueur():
    """on edmande au joueur d'entrer  son nom.
    le nom devra etre supérieur à 3 caractères .
    """
    nom_joueur = input("\n\nVeuillez entrer votre nom : ")
    
    # pour les if/then/else court tu peux utiliser cette notation
    return select_joueur() if len(nom_joueur)<3 else nom_joueur

    
def get_score():
    """on va récupérer les scores de tous les joueurs
    """
    if os.path.exists(score_file): # test de l'existence du fichier
        # L'instruction with assure un 'close' dans TOUS les cas
        with open(score_file, 'rb') as fichier_score:
            depickler = pickle.Unpickler(fichier_score)
            data_score = depickler.load()
        # Une autre variante encore plus courte :
        #data_score = pickle.load(open(score_file, 'rb'))
    else:
        data_score = {}
        
    return data_score

def show_score(name, score):
    """On récupère le nom du joueur ainsi que le dictionnaire des scores.
    si le score existe, on l'affiche
    """
    try:
        score[name]
    except KeyError:
        score[name] = 0
    
    score_joueur =  score[name]
    print("\nVotre précedent score etait de",score_joueur,"!")
    
    return score_joueur

def save_score(score):        
    pickle.dump(score, open(score_file, 'wb'))

def select_mot():
    """on choisi un mot au hasard dans la liste"""
    mot_choisi = choice(liste_mots)
    return mot_choisi

def cache_mot(mot, caracteres):
    """on cache le mot en fonction de ce que le joueur a dja trouvé
    mot (str) => mot a cacher 
    caracteres (list) => liste de lettre que le joueur a tapé
    """
    mot_cache = [lettre if lettre in caracteres else '#' for lettre in mot]
    
    return mot_cache

def saisir_lettre():
    """on récupère la lettre saisi par le joueur
    """
    valeur = input("\nVeuillez taper une lettre : ")
    valeur = valeur.lower()
    
    
    if len(valeur)>1 or not valeur.isalpha():
        print("\ncaractère non valide...")
        return saisir_lettre()
    else:
        print("\nVous avez taper",valeur)
        return valeur
   
    
    
    



    
    
    
    
