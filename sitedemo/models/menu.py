# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.title = 'Bienvenue'
response.subtitle = T('Sur mon Site!')

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Guillaum Bourbon'
response.meta.description = 'a test app'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'
response.meta.copyright = 'Copyright 2011'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Accueil'), False, URL('default','index'), []),
    
    (T('Documents'), False, URL('default','index'), [
        (T('Histoire'), False, URL('default','history'), []),
        (T('Description'), False, URL('default','description'), [])
    ]),
    
    (T('Graphiques'), False, URL('default','index'), [
        (T('Chiffres Clés'), False, URL('default','cle'), []),
        (T('Evolution'), False, URL('default','evolution'), [])
    ])
    
]
