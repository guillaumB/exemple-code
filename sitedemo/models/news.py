# coding: utf8


db = DAL('sqlite://storage.db')

db.define_table('news',
    Field('title'),
    Field('content')
)
