# -*- coding: utf-8 -*-
from random import randrange
from math import ceil

def index():
    #variables de depart
    nbofplay = session.counter
    msg = ''
    numero = (request.vars.numero or -1)
    mise = (request.vars.mise or 0)
    money = (session.money or 1000)
        
    
    #on converti en nombre ce que le joueur a tapé 
    try:
        numero = int(numero)
    except ValueError:
        msg = 'Vous n\'avez pas entrer de numéro'  
    
    #on verifie que le nombre soit bien compris entre 0 et 49
    if numero<0 or numero>49:
        msg = 'Vous n\'avez entrer un nombre entre 0 et 49'
    else:    
        #on converti la mise du joueur
        try:
            mise = int(mise)
        except ValueError:
            msg = 'Vous n\'avez pas entrer de mise'  
    
        #on verifie que la mise soit comprise en 0 et ce que peut miser le joueur
        # ce n'est pas ce que disait ton code !!!
        #if money<=0 or money<mise:
        if 0 <= money < mise:
            msg = 'Vous ne pouvez pas miser cette somme !'
        else:
            #lancement de la roulette
            gagnant = randrange(50)
    
            #on teste si le joueur a gagné... ou pas !
            if gagnant == numero:
                msg = 'Félicitations ! Vous gagnez '+str(mise*3)+' $'
                money += mise*3
            elif gagnant%2 == numero%2: #meme couleur
                msg = 'Vous avez miser sur la bonne couleur ! Vous gagnez '+str(mise)+' $'
                money += mise
            else:
                msg = 'Vous avez perdu, désoler ...'
                money -= mise
                
            session.money = money 
    
    #on afficher jouer ou rejouer en fonction du nombre de partie
    if nbofplay == 1:
        play = INPUT(_type='submit', _value='Jouer', _id='play')
    else:   
        play = INPUT(_type='submit', _value='Rejouer', _id='play') 
                
    #on teste pour voir si le joueur a encor de l'argent
    if money>0:
        money = str(money)+' $'
    else:
        money = 'vous n\'avez plus d\'argent ...'
        
    #return dict(money=money, play=play, msg=msg)
    # une façon plus brutale de passer les données à la vue
    return locals()
    
